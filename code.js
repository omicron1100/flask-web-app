//URL extension
var urlBase = 'http://group6pcm.xyz:5000';

var userId = 0;
var firstName = "";
var lastName = "";

//Login function
function doLogin(e)
{
	if (e && e.keyCode != 13) return;
	
	//set global variables to 0
	userId = 0;
	firstName = "";
	lastName = "";

	//grabs inserted user values from html
	var login = document.getElementById("loginName").value;
	var password = md5(document.getElementById("loginPassword").value);
 
 
  document.getElementById("loginResult").innerHTML = "";
 
 //If nothing is inserted then print err msg
  if (!login || !password) {
    document.getElementById("loginResult").innerHTML = "<br />User/Password Cannot be Empty";
		return;
  }
	//var hash = md5( password );

	//package json's
	var jsonPayload = '{"Login" : "' + login + '", "Password" : "' + password + '"}';
	var url = urlBase + '/login';


	var xhr = new XMLHttpRequest();
	//opening connection to server
	xhr.open("POST", url, true);

	xhr.setRequestHeader("Content-type", "application/json; charset=UTF-8");
	try
	{
		xhr.onreadystatechange = function()
		{
			if (this.readyState == 4 && this.status == 200)
			{
				//retrieve Json
				var jsonObject = JSON.parse( xhr.responseText );
				//prints error msg if login does not match
				if( !jsonObject || jsonObject.userId < 1 )
				{
					document.getElementById("loginResult").innerHTML = "<br />User/Password Combination Incorrect";
					return;
				}

				firstName = jsonObject.FirstName;
				lastName = jsonObject.LastName;
 			  userId = jsonObject.ID;
		//cookies are saves and login was successful
				saveCookie();
        closePopUps();
        updateLogin();
				
			}
		};
		xhr.send(jsonPayload);
	}
	catch(err)
	{
		document.getElementById("loginResult").innerHTML = "<br />" + err.message;
	}

}

//Signup function
function doSignUp(e)
{
  //set global variables to 0
  if (e && e.keyCode != 13) return;
	userId = 0;
	firstName = "";
	lastName = "";
	//grabs inserted user values from html
	var emailField = document.getElementById("register-email");
	var firstn = document.getElementById("register-first-name").value;
	var lastn = document.getElementById("register-last-name").value;
	var email = emailField.value;
	var login = document.getElementById("register-username").value;
	var password = md5(document.getElementById("register-password").value);
  var passwordConfirm = md5(document.getElementById("register-password-confirm").value);

	document.getElementById("signUpResult").innerHTML = "";
  document.getElementById("signUpResult").style.color = "#F00";
  
   //If nothing is inserted then print err msg
  if (!firstn || !lastn || !email || !login || !password) {
    document.getElementById("signUpResult").innerHTML = "<br />Fields cannot be empty!";
    return;
  }
   //Check if email is valid
  if (!emailField.validity.valid) {
    document.getElementById("signUpResult").innerHTML = "<br />Invalid Email Address!";
    return;
  }
   //Check to see of passwords entered match
  if (password != passwordConfirm) {
    document.getElementById("signUpResult").innerHTML = "<br />Passwords do not match!";
    return;
  }

	// var jsonPayload = '{"login" : "' + login + '", "password" : "' + hash + '"}';

  	// Json oackage to be sent out
	var jsonPayload = '{"FirstName" : "' + firstn + '","LastName" : "' + lastn + '","Email" : "' + email + '", "Login" : "' + login + '", "Password" : "' + password + '"}';
	var url = urlBase + '/signup';


	var xhr = new XMLHttpRequest();
	// opening connection to server
	xhr.open("POST", url, true);

	xhr.setRequestHeader("Content-type", "application/json; charset=UTF-8");
	try
	{
		xhr.onreadystatechange = function()
		{
			if (this.readyState == 4 && this.status == 200)
			{
				var jsonObject = JSON.parse( xhr.responseText );
        console.log(jsonObject);
				userId = jsonObject.id;
				//error if user already exist
				if( userId == -1 )
				{
					document.getElementById("signUpResult").innerHTML = "<br />User Already Exists!";
					return;
				}

				firstName = jsonObject.firstName;
				lastName = jsonObject.lastName;
		//cookies are saved 
				saveCookie();
        setTimeout(() => {updateLogin();}, 4.0*1000);
        document.getElementById("signUpResult").style.color = "#070";
        document.getElementById("signUpResult").innerHTML = "<br />Registration Successful";
        //closePopUps();
	      //change for users
				//window.location.href = "home.html";

			}
		};
		xhr.send(jsonPayload);
	}
	catch(err)
	{
		document.getElementById("signUpResult").innerHTML = "<br />" + err.message;
	}

}

//function to save cookies
function saveCookie()
{
	var minutes = 20;
	var date = new Date();
	date.setTime(date.getTime()+(minutes*60*1000));
	document.cookie = "firstName=" + firstName + ",lastName=" + lastName + ",userId=" + userId + ";expires=" + date.toGMTString();
}
//function that allows you to read cookies
function readCookie()
{
	userId = -1;
	var data = document.cookie;
  console.log(data);
	var splits = data.split(",");
	for(var i = 0; i < splits.length; i++)
	{
		var thisOne = splits[i].trim();
		var tokens = thisOne.split("=");
		if( tokens[0] == "firstName" )
		{
			firstName = tokens[1];
		}
		else if( tokens[0] == "lastName" )
		{
			lastName = tokens[1];
		}
		else if( tokens[0] == "userId" )
		{
			userId = parseInt( tokens[1].trim() );
		}
	}
   console.log(document.cookie);

   console.log("read" + userId);

	if( userId < 0 )
	{
		window.location.href = "index.html";
    return 0;
	}
	else
	{
    //window.location.href = "manage-contacts.html";
		document.getElementById("userName").innerHTML = "Logged in as " + firstName + " " + lastName;
	}
    return userId;
}
//Logout function
function doLogout()
{
	userId = 0;
	firstName = "";
	lastName = "";
	document.cookie = "firstName= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
  closePopUps();
  updateLogin();
	//window.location.href = "index.html";
}

//Add new contact
function addContact(e) {
  if (e && e.keyCode != 13) return; 
  //grabs inserted user values from html
	var fn = document.getElementById("add-first-name").value;
	var ln = document.getElementById("add-last-name").value;
	var emailField = document.getElementById("add-email");
  var email = emailField.value; 
	var phoneField = document.getElementById("add-phone");
  var phone = phoneField.value;
 
	document.getElementById("addContactResult").innerHTML = "";
  document.getElementById("addContactResult").style.color = "#F00";
  
  //If nothing is inserted then print err msg
  if (!fn || !ln || !email || !phone) {
    document.getElementById("addContactResult").innerHTML = "<br />Fields cannot be empty!";
    return;
  }
  //Check if email is valid
  if (!emailField.validity.valid) {
    document.getElementById("addContactResult").innerHTML = "<br />Invalid Email Address!";
    return;
  }
  
  /*if (!phoneField.validity.valid) {
    document.getElementById("addContactResult").innerHTML = "<br />Invalid Phone Number!";
    return;
  }
  
  phone = phone.replace('-', '');*/
  //packagethe JSON 
  var jsonPayload = '{"UserID" : "' + userId + '","FirstName" : "' + fn + '","LastName" : "' + ln + '","Email" : "' + email + '", "Phone" : "' + phone + '"}';
	var url = urlBase + '/add_contact';

	var xhr = new XMLHttpRequest();
	xhr.open("POST", url, true);
	xhr.setRequestHeader("Content-type", "application/json; charset=UTF-8");
	try
	{
		xhr.onreadystatechange = function()
		{
			//successfuly added the contacts
			if (this.readyState == 4 && this.status == 200)
			{
        document.getElementById("addContactResult").style.color = "#070";
				document.getElementById("addContactResult").innerHTML = "<br />Contacts has been added";
			}
		};
		xhr.send(jsonPayload);
	}
	catch(err)
	{
		document.getElementById("addContactResult").innerHTML = "<br />" + err.message;
	}

}

function search(e)
{
  if (e && e.keyCode != 13) return;
  //set var to empty
	var search = "";
	search = document.getElementById("searchText").value;
	document.getElementById("searchResult").innerHTML = "";

	var contactList = "";
		if(search == null)
			search = "";
    //package search Json
	var jsonPayload = '{"search" : "' + search + '","userID" : ' + userId + '}';
	var url = urlBase + '/search';

	var xhr = new XMLHttpRequest();
	xhr.open("PUT", url, true);
	xhr.setRequestHeader("Content-type", "application/json; charset=UTF-8");
	try
	{
		xhr.onreadystatechange = function()
		{
			if (this.readyState == 4 && this.status == 200)
			{
				//document.getElementById("searchResult").innerHTML = "Contacts retrieved";

          var jsonObjectList = JSON.parse( xhr.responseText );

          console.log(jsonObjectList);
		//placed the recieved json into an array
          for (let i = 0; i < jsonObjectList.length; i++) {
            addContactCard(
              jsonObjectList[i].FirstName,
              jsonObjectList[i].LastName,
              jsonObjectList[i].ID,
              jsonObjectList[i].Email,
              jsonObjectList[i].Phone,
              jsonObjectList[i].DateCreated
            );
          }

				//document.getElementsByTagName("p")[0].innerHTML = contactList;
			}
		};
		xhr.send(jsonPayload);
	}
	catch(err)
	{
		document.getElementById("searchResult").innerHTML = err.message;
	}

}

//function that allows for drop down search cards
function addContactCard(firstName, lastName, id, email, phone, date) {
  document.getElementById("searchResult").innerHTML += `
    <div class="contact-card" id="contact-user-${id}">
          <div class="contact-card-title" onclick="toggleContactCard(this.parentElement.id)">
            <span class="contact-card-title-first-name">${firstName}</span>
            <span class="contact-card-title-last-name">${lastName}</span>
          </div>
          <div class="contact-card-body">
            <div class="contact-card-body-fields">
              <strong>
                <p>Email:</p>
                <p>Phone:</p>
                <p>Date Created:</p>
              </strong>
            </div>
            <div class="contact-card-body-values">
              <p class="email-value">${email}</p>
              <p class="phone-value">${phone}</p>
              <p class="date-value">${date}</p>
            </div>
            <br />
            <input class="edit-contact" type="button" value="Edit" onclick="editContact(this.parentElement.parentElement.id)" />
            <input class="delete-contact" type="button" value="Delete" onclick="deleteContact(this.parentElement.parentElement.id)" />
            <input class="delete-confirm" type="button" value="Are Ya Sure?" />
            <input class="edit-confirm" type="button" value="Confirm" />
            <input class="edit-cancel" type="button" value="Cancel" />
          </div>
        </div>
  `;
}
//the toggle function for the drop down card
function toggleContactCard(id) {
  let card = document.getElementById(id);
  
  let title = card.getElementsByClassName("contact-card-title")[0];
  console.log(title.getElementsByClassName("contact-card-title-first-name")[0].tagName);
  if (title.getElementsByClassName("contact-card-title-first-name")[0].tagName == "INPUT") return;
  
  let body = card.getElementsByClassName("contact-card-body");
  if (body[0].style.height == "12rem") {
    body[0].style.height = "0rem";
  } else {
    body[0].style.height = "12rem";
  }
}
//Delete function 
function deleteContact(id) {
  let card = document.getElementById(id);
  //swap id value
  id = id.replace("contact-user-", "");
  
  let deleteButton = card.getElementsByClassName("delete-contact")[0];
  let confirmDeleteButton = card.getElementsByClassName("delete-confirm")[0];
  
  deleteButton.style.display = "none";
  confirmDeleteButton.style.display = "inline";
  
  setTimeout(() => {
    deleteButton.style.display = "inline";
    confirmDeleteButton.style.display = "none";
  }, 4.0*1000);
  
  confirmDeleteButton.addEventListener("click", () => {
    var jsonPayload = '{"ID" : "' + id + '"}';
  	var url = urlBase + '/delete';
  	var xhr = new XMLHttpRequest();
  	xhr.open("DELETE", url, true);
  	xhr.setRequestHeader("Content-type", "application/json; charset=UTF-8");
  	try
  	{
		//confirm contact is deleted
  		xhr.onreadystatechange = function()
  		{
  			if (this.readyState == 4 && this.status == 200)
  			{
           card.remove();
  			}
  		};
  		xhr.send(jsonPayload);
  	}
  	catch(err)
  	{
  		//document.getElementById("addContactResult").innerHTML = "<br />" + err.message;
  	}
  });
}
//edit contact function
function editContact(id) {
  let card = document.getElementById(id);
  id = id.replace("contact-user-", "");
  
  let title = card.getElementsByClassName("contact-card-title")[0];
  let values = card.getElementsByClassName("contact-card-body-values")[0];
  //new fields inserted
  let firstNameField = title.getElementsByClassName("contact-card-title-first-name")[0];
  let lastNameField = title.getElementsByClassName("contact-card-title-last-name")[0];
  let emailField = values.getElementsByClassName("email-value")[0];
  let phoneField = values.getElementsByClassName("phone-value")[0];
  let date = values.getElementsByClassName("date-value")[0].innerHTML;
  //swaping old field with new field
  let oldFirstName = firstNameField.innerHTML;
  let oldLastName = lastNameField.innerHTML;
  let oldEmail = emailField.innerHTML;
  let oldPhone = phoneField.innerHTML;
  //Edit buttons set
  let confirmButton = card.getElementsByClassName("edit-confirm")[0];
  let cancelButton = card.getElementsByClassName("edit-cancel")[0];
  
  card.getElementsByClassName("edit-contact")[0].style.display = "none";
  card.getElementsByClassName("delete-contact")[0].style.display = "none";
  confirmButton.style.display = "inline";
  cancelButton.style.display = "inline";
  
  title.innerHTML = `
    <input type="text" class="contact-card-title-first-name" placeholder="First Name" value="${oldFirstName}" />
    <input type="text" class="contact-card-title-last-name" placeholder="Last Name" value="${oldLastName}" />
  `;
  
  values.innerHTML = `
    <input type="email" class="email-value" placeholder="email@domain.com" value="${oldEmail}"/>
    <br />
    <input type="tel" class="phone-value" placeholder="Phone Number" value="${oldPhone}"/>
    <p class="date-value">${date}</p>
  `;
  
  cancelButton.addEventListener("click", () => {
    title.innerHTML = `
      <span class="contact-card-title-first-name">${oldFirstName}</span>
      <span class="contact-card-title-last-name">${oldLastName}</span>
    `;
    values.innerHTML = `
      <p class="email-value">${oldEmail}</p>
      <p class="phone-value">${oldPhone}</p>
      <p class="date-value">${date}</p>
    `;
    card.getElementsByClassName("edit-contact")[0].style.display = "inline";
    card.getElementsByClassName("delete-contact")[0].style.display = "inline";
    confirmButton.style.display = "none";
    cancelButton.style.display = "none";
  });
  //set confirm button for editing
  confirmButton.addEventListener("click", () => {
    firstNameField = title.getElementsByClassName("contact-card-title-first-name")[0];
    lastNameField = title.getElementsByClassName("contact-card-title-last-name")[0];
    emailField = values.getElementsByClassName("email-value")[0];
    phoneField = values.getElementsByClassName("phone-value")[0];
    date = values.getElementsByClassName("date-value")[0].innerHTML;
    let firstName = firstNameField.value;
    let lastName = lastNameField.value;
    let email = emailField.value;
    let phone = phoneField.value;
    
    var jsonPayload = '{"FirstName" : "' + firstName + '","LastName" : "' + 
                      lastName + '","userID" : "' + userId + '","Email" : "' + email + 
                      '","Phone" : "' + phone + '","contactID" : "' + id + '"}';
    console.log(jsonPayload);
  	var url = urlBase + '/edit_contact';
  	var xhr = new XMLHttpRequest();
  	xhr.open("PUT", url, true);
  	xhr.setRequestHeader("Content-type", "application/json; charset=UTF-8");
  	try
  	{
  		xhr.onreadystatechange = function()
  		{
  			if (this.readyState == 4 && this.status == 200)
  			{
             title.innerHTML = `
              <span class="contact-card-title-first-name">${firstName}</span>
              <span class="contact-card-title-last-name">${lastName}</span>
             `;
             values.innerHTML = `
              <p class="email-value">${email}</p>
              <p class="phone-value">${phone}</p>
              <p class="date-value">${date}</p>
             `;
              card.getElementsByClassName("edit-contact")[0].style.display = "inline";
              card.getElementsByClassName("delete-contact")[0].style.display = "inline";
              confirmButton.style.display = "none";
              cancelButton.style.display = "none";
  			}
  		};
  		xhr.send(jsonPayload);
  	}
  	catch(err)
  	{
  		//document.getElementById("addContactResult").innerHTML = "<br />" + err.message;
  	}
  });
}
//close the toggle pop ups
function closePopUps() {
  let popups = document.getElementsByClassName("popup");
  for (let i = 0; i < popups.length; i++) {
    popups[i].style.display = "none";
  }
  document.getElementById("blurb").style.color = "#003E41";
}
//these next five functions allows for toggles to be display
function toggleLogin() {
  let loginDiv = document.getElementById("loginDiv");
  let dis = loginDiv.style.display;
  closePopUps();
	loginDiv.style.display = dis == "none" ? "block" : "none";
}

function toggleRegister() {
  let registerDiv = document.getElementById("registerDiv");
  let dis = registerDiv.style.display;
  closePopUps();
	registerDiv.style.display = dis == "none" ? "block" : "none";
}

function toggleAddUser() {
  let adduserDiv = document.getElementById("adduserDiv");
  let dis = adduserDiv.style.display;
  closePopUps();
	adduserDiv.style.display = dis == "none" ? "block" : "none";
}
function toggleSearch() {
  let searchDiv = document.getElementById("searchDiv");
  let dis = searchDiv.style.display;
  closePopUps();
	searchDiv.style.display = dis == "none" ? "block" : "none";
}
function toggleAboutUs() {
  let aboutUsDiv = document.getElementById("aboutUsDiv");
  let dis = aboutUsDiv.style.display;
  closePopUps();
	aboutUsDiv.style.display = dis == "none" ? "block" : "none";
}
//Update the login page to the what you can consider the home page
function updateLogin() {
  /*/document.getElementById("menu-SignedIn").style.display = "block";
  document.getElementById("menu-newUser").style.display = "none";
  /**/
  if (userId > 0) {
    document.getElementById("menu-SignedIn").style.display = "block";
    document.getElementById("menu-newUser").style.display = "none";
    let blurb = document.getElementById("blurb");
    blurb.innerHTML = `Hello, ${firstName} ${lastName}`;
    setTimeout(() => {
      blurb.style.color = "#000";
    }, 0.5*1000);
    
  } else {
    closePopUps();
    document.getElementById("loginDiv").style.display = "block";
    document.getElementById("menu-SignedIn").style.display = "none";
    document.getElementById("menu-newUser").style.display = "block";
    document.getElementById("blurb").innerHTML = "";
  }
/**/}

/*
var toggleLogin = document.getElementById("login-open-popup");
	toggleLogin.addEventListener("click", function(e) {
		let loginDiv = document.getElementById("loginDiv");
    let dis = loginDiv.style.display;
    closePopUps();
		loginDiv.style.display = dis == "none" ? "block" : "none";
	});
 
 var toggleRegister = document.getElementById("register-open-popup");
	toggleRegister.addEventListener("click", function(e) {
    let registerDiv = document.getElementById("registerDiv");
    let dis = registerDiv.style.display;
    closePopUps();
		registerDiv.style.display = dis == "none" ? "block" : "none";
	});
 #menu-newUser {
  display: none;
}

#menu-SignedIn {
  display: block;
}

<input id="register-password-confirm" class="loginFields" type="password" placeholder="Confirm Password" onkeyup="doSignUp(event)" />
        <i class="fas fa-eye-slash inline-icon" id="showRegisterPasswordConfirm"></i>
 
*/

window.onload = () => {
  updateLogin();
  var mainMenuButton = document.getElementById("navBar-menu-button");
	mainMenuButton.addEventListener("click", function(e) {
		var sidebar = document.getElementById("main-sidebar");
		if (sidebar.style.width == "0%") {
			sidebar.style.width = "10%";
		} else {
			sidebar.style.width = "0%";
		}
	});

	var passwordLoginIcon = document.getElementById("showLoginPassword");
	passwordLoginIcon.addEventListener("click", function(e) {
		var passwordField = document.getElementById("loginPassword");
		passwordField.type = passwordField.type == "password" ? "text" : "password";
		passwordLoginIcon.classList.toggle("fa-eye");
		passwordLoginIcon.classList.toggle("fa-eye-slash");
	});
 
  var passwordRegisterIcon = document.getElementById("showRegisterPassword");
	passwordRegisterIcon.addEventListener("click", function(e) {
		var passwordField = document.getElementById("register-password");
		passwordField.type = passwordField.type == "password" ? "text" : "password";
		passwordRegisterIcon.classList.toggle("fa-eye");
		passwordRegisterIcon.classList.toggle("fa-eye-slash");
	});
 
  var passwordRegisterConfirmIcon = document.getElementById("showRegisterPasswordConfirm");
	passwordRegisterConfirmIcon.addEventListener("click", function(e) {
		var passwordField = document.getElementById("register-password-confirm");
		passwordField.type = passwordField.type == "password" ? "text" : "password";
		passwordRegisterConfirmIcon.classList.toggle("fa-eye");
		passwordRegisterConfirmIcon.classList.toggle("fa-eye-slash");
	});
}
